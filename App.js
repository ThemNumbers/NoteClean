import React from 'react';
import {StyleSheet, Text, View } from 'react-native';
import {NativeRouter, Switch, Route } from "react-router-native";
import {createStackNavigator} from 'react-navigation'

import Auth from './src/components/Auth'
import Register from './src/components/Register'

const Navigation = createStackNavigator({ 
  AuthScreen: { screen: Auth },
  RegisterScreen: { screen: Register }, 
})
export default Navigation;

// export default class App extends React.Component {
//   render() {
//     console.log(history)
//     return (
//       <NativeRouter>
//         <View style={styles.container}>
//           <Switch>
//             <Route exact path="/" component={Auth} />
//             <Route path="/auth" component={Auth} />
//             <Route path="/register" component={Register} />
//           </Switch>
//         </View>
//       </NativeRouter>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     backgroundColor: 'aquamarine',
//   },
// });