import {Service} from '../services/Service'
import { userConst } from '../constants/userConst'

export const Actions = {
    login,
    register,
};

function register(email, password) {{
        request();

        Service.register(email, password)
            .then(
                user => {
                    console.log(user)
                    if (typeof (user) !== 'undefined') {
                        success();
                    }
                },
                error => {
                    failure(error);
                    console.log(error)
                }
            );
}

    function request() { return { type: userConst.REGISTER_REQUEST } }
    function success() { return { type: userConst.REGISTER_SUCCESS } }
    function failure(error) { return { type: userConst.REGISTER_FAILURE, error } }
}


function login(email, password) {{
        request();

        Service.login(email, password)
            .then(
                user => {
                    if (typeof (user) !== 'undefined') {
                        success(user);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)

                }
            );
    }

    function request() { return { type: userConst.LOGIN_REQUEST } }
    function success(user) { return { type: userConst.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConst.LOGIN_FAILURE, error } }
}