import Axios from 'axios';

export const Service = {
    login,
    register,
};

let defaultURL = 'http://localhost:3000/api/'

function login(email, password) {
    return Axios.post( defaultURL +'Users/login', {
        email: email,
        password: password
    })
    .then(function(response){
        localStorage.setItem('token', JSON.stringify(response.data.id));
        console.log(response)
        //history.push('/main');
        alert('all good')
        return response
    })
    .catch(function (error) {
        console.log(error);
        alert('Неправильный логин или пароль!')
      });
}

function register(email, password) {
    return Axios.post(defaultURL + 'Users' ,  {
        email: email,
        password: password
    })
    .then(function(response){
        //history.push('/auth');
        alert('all good')
        console.log(response)
    })
    .catch(function (error) {
        console.log(error);
        alert('Данный пользователь уже существует или введен неправильный email!')
      });
}