import React from 'react';
import { Alert, StyleSheet, Text, TextInput, Button, View } from 'react-native';
import { createMemoryHistory} from 'history';
import { Actions } from '../actions/Actions'
 
export const history = createMemoryHistory();

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: "aquamarine",
      elevation: null
    },
  };

  onBtnRegister() {
    const { email, password } = this.state;
    console.log(email, password); 
    if (email && password) {
       Actions.register(email, password);   
    } else
      Alert.alert('Поля не могут быть пустыми') 
  }

  // onBtnBack() {
  //   history.push("/auth")
  //   console.log(history)
  // }

  render() {
    let {navigate} = this.props.navigation
    return (
      <View style={styles.container}>
        <Text style={styles.header}>NOTES</Text>
        <Text>Регистрация</Text>
        <TextInput
          style={styles.inputbox}
          placeholder="Email"
          onChangeText={(email) => this.setState({email})}
        />
        <TextInput
          style={styles.inputbox}
          placeholder="Пароль"
          secureTextEntry
          onChangeText={(password) => this.setState({password})}
        />
        <View style={styles.buttonContainer}>
          <Button
            onPress={this.onBtnRegister.bind(this)}
            title="Зарегестрироваться"
            color="#f1841e"
          />
        </View>
        {/* <View style={{marginBottom: 1}}>
          <Button
            onPress={() => navigate(navigate.goBack(), {})}
            title="<----"
            color="#6383a8"
          /> 
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'aquamarine',
  },
  header: {
    color: '#f1841e',
    fontWeight: 'bold',
    fontSize: 30,
    paddingTop: 50,
    marginBottom: 110,
    shadowColor: '#f8983f',
  },
  inputbox: {
    padding: 5,
    width: 200,
  },
  buttonContainer: {
    marginTop: 7,
    marginBottom: 5,
  },
});
